<?php

namespace Mindgeek\Domain\Model;

class EstimationLogEntry
{
    /** @var string */
    private $description;

    /** @var int */
    private $hoursRemaining;

    /** @var string */
    private $name;

    /** @var string */
    private $volunteer;

    /**
     * EstimationLogEntry constructor.
     * @param string $description
     * @param int    $hoursRemaining
     * @param string $name
     * @param string $volunteer
     */
    private function __construct(string $description, int $hoursRemaining, string $name, string $volunteer)
    {
        $this->description    = $description;
        $this->hoursRemaining = $hoursRemaining;
        $this->name           = $name;
        $this->volunteer      = $volunteer;
    }

    public static function create(string $description, int $hoursRemaining, string $name, string $volunteer)
    {
        return new self($description, $hoursRemaining, $name, $volunteer);
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function hoursRemaining(): int
    {
        return $this->hoursRemaining;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function volunteer(): string
    {
        return $this->volunteer;
    }
}