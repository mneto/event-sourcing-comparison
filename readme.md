# Event Sourcing Comparison

## Objective

Event Sourcing is a different way to approach developing systems. It does not come without its share of challenges, so the purpose
of this repository is to provide a sample implementation of the same issue using this pattern.

## The Problem

I selected one issue described in the implementing Domain Driven Design, by Vaughn Vernon, chapter 10, Aggregates.

On that chapter aggregate design is mentioned and one solution is the creation of aggregates:
- Release
- Sprint
- Product
- BackLogItem

BacklogItem contains:
- BacklogId (VO)
- status
- story
- storyPoints
- summary
- type
- ProductId (VO)
- ReleaseId (VO)
- SprintId (VO)
- Tasks (0..*)

Task is an entity that contains:
- description
- hoursRemaining
- name
- volunteer
- estimationLogEntries (0..*) (VO)

EstimationLogEntry is a VO that contains:
- description
- hoursRemaining
- name
- volunteer

Release, Sprint and Product are not relevant for the scope of this example and will be referenced by their id.

## The implementation

I decided to do two parallel implementations, each using a different library that supports several aspects of ES:
- Prooph
- Broadway

On both the following aspects are to be considered:
- The setup needed (event and command handlers)
- How to fire the events
- How to handle complex aggregates (with inner entities)
- The event store used
- How to deal with projections (synchronous or asynchronous)
- How to deal with snapshot (for performance)
- How to deal with private streams (example, to obfuscate/encrypt password or sensitive information)

## Setup

I used Laravel and equivalent laravel-packages and to make it easier the process, laradock.io Docker environment.

- Clone this repository
- Initialize the submodules
```
git submodule init
git submodule update
```

If that does not work, use
```
git submodule add https://github.com/Laradock/laradock.git
```

- Copy the sample .env file for the docker environment
```
cp .env.laradock laradock\.env
```

We are using Docker standard IP 10.0.75.1 so if your machine has a different one please update.

- Copy the example .env for the laravel app
```
cp .env.example .env
```

- Start the docker images
```
cd laradock
docker-compose up -d nginx mysql workspace elasticsearch kibana
```

It will take some time in the beginning to download the images

- Install the composer packages
```
cd laradock
docker-compose exec workspace bash
# composer install
```

- Generate the key
```
# artisan key:generate
```



## Notes

As you switch from one branch to another you may have to run 
```
# composer dump-autoload
```

So it can create the proper autoload class map.

And

```
# artisan migrate:refresh
```

To drop and recreate the necessary tables.

The following routes are available - added as GET for simplicity

http://localhost/api/backlog-item - to add a backlog item

http://localhost/api/backlog-item/{id}/task - to add a task to a backlog item

http://localhost/api/backlog-item/{id}/task/{task-id}/estimate - to add an estimation to a task of a backlog item

## Prooph

It is split into several components with an event store with support for MySQL, Postgresql and MariaDB out of the box.
The event sourcing aspect is handled by a package that will be deprecated by Dec 2019.

We can go with their event store implementation and have our own event sourcing support.

- The setup needed (event and command handlers)

It does not need to use command bus. It provides an event bus for the events when using event sourcing.

- How to fire the events

There are facilities from base classes to record the events

```php
$obj->recordThat(BacklogItemWasCreated::withData(
            $id,
            $productId,
            $status,
            $story,
            $storyPoints,
            $summary,
            $type
        ));
```

Always fired from the aggregate root.

- How to handle complex aggregates (with inner entities)

See the BacklogItem/Task/EstimatedLogEntry. You would trigger changes on the inner entities from
the Aggregate Root.

- The event store used

Supports one stream per aggregate id or not.

- How to deal with projections (synchronous or asynchronous)

Can be both. If you connect the projector to react on the event it will be sync. If you have a worker
you can have it consuming the event stream instead of the domain event as it is generated 

- How to deal with snapshot (for performance)

There is a package snapshooter that allows, similar to the projections, to be executed as a worker
and save.
If you pass the snapshot to the repository it will try to load the snapshot and replay just the events
that happened after that.

- How to deal with private streams (example, to obfuscate/encrypt password or sensitive information)

You have the concept of Plugins and Metadata Enricher that would allow you to add or modify information
on the event.

It has been suggested, but not verified by me, to create a persistence strategy and message factory to
deal with that.

Example. Instead of this
```php
$eventStore = new MySqlEventStore(new FQCNMessageFactory(), $pdo, new MySqlAggregateStreamStrategy());
```

Use a custom MessageFactory that would deal with decryption.

### Projections

To run the projection you have to run it from the command line
```
#artisan project:backlog-item
```

It will keep running on the foreground. In practice you would likely use something like supervistord.

### Snapshots

To run the snapshots you have to run it from the command line
```
#artisan snapshot:backlog-item
```

It will keep running on the foreground. In practice you would likely use something like supervistord.
