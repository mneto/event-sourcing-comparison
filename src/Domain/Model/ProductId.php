<?php

namespace Mindgeek\Domain\Model;

use Ramsey\Uuid\Uuid;

class ProductId
{
    private $value;

    private function __construct($value)
    {
        $this->value = $value ?: Uuid::uuid4();
    }

    public static function create($value)
    {
        return new self($value);
    }

    public static function createFromString(string $value)
    {
        return new self(Uuid::fromString($value));
    }

    /**
     * @return \Ramsey\Uuid\UuidInterface
     */
    public function value(): \Ramsey\Uuid\UuidInterface
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value()->toString();
    }
}