<?php

namespace Mindgeek\Domain\Model;

class Task
{
    /** @var TaskId */
    private $id;

    /** @var string */
    private $description;

    /** @var int */
    private $hoursRemaining;

    /** @var string */
    private $name;

    /** @var string */
    private $volunteer;

    /** @var EstimationLogEntry[] */
    private $estimations;

    private function __construct(TaskId $id, string $description, int $hoursRemaining, string $name, string $volunteer)
    {
        $this->id = $id;
        $this->description = $description;
        $this->hoursRemaining = $hoursRemaining;
        $this->name = $name;
        $this->volunteer = $volunteer;
        $this->estimations = [];
    }

    public static function create(TaskId $id, string $description, int $hoursRemaining, string $name, string $volunteer)
    {
        return new self($id, $description, $hoursRemaining, $name, $volunteer);
    }

    public function addEstimate(EstimationLogEntry $estimate)
    {
        $this->estimations[] = $estimate;
    }

    /**
     * @return TaskId
     */
    public function id(): TaskId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function hoursRemaining(): int
    {
        return $this->hoursRemaining;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function volunteer(): string
    {
        return $this->volunteer;
    }

    /**
     * @return EstimationLogEntry[]
     */
    public function estimations(): array
    {
        return $this->estimations;
    }
}