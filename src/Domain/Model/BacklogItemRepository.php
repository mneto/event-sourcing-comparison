<?php

namespace Mindgeek\Domain\Model;

interface BacklogItemRepository
{
    public function save(BacklogItem $backlogItem): void;

    public function get(string $id): ?BacklogItem;
}