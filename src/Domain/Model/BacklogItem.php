<?php

namespace Mindgeek\Domain\Model;

class BacklogItem
{
    /** @var BacklogItemId */
    private $id;

    /** @var string */
    private $status;

    /** @var string */
    private $story;

    /** @var int */
    private $storyPoints;

    /** @var string */
    private $summary;

    /** @var string */
    private $type;

    /** @var Task[] */
    private $tasks;

    /** @var ProductId */
    private $productId;

    /** @var SprintId */
    private $sprintId;

    /** @var ReleaseId */
    private $releaseId;

    /**
     * @return BacklogItemId
     */
    public function id(): BacklogItemId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function status(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function story(): string
    {
        return $this->story;
    }

    /**
     * @return string
     */
    public function summary(): string
    {
        return $this->summary;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }

    /**
     * @return Task[]
     */
    public function tasks(): array
    {
        return $this->tasks;
    }

    /**
     * @return ProductId
     */
    public function productId(): ProductId
    {
        return $this->productId;
    }

    /**
     * @return SprintId
     */
    public function sprintId(): SprintId
    {
        return $this->sprintId;
    }

    /**
     * @return ReleaseId
     */
    public function releaseId(): ReleaseId
    {
        return $this->releaseId;
    }

    /**
     * @return int
     */
    public function storyPoints(): int
    {
        return $this->storyPoints;
    }
}